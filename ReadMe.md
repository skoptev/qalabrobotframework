## Install Robot Framework

* Install Python
* Install framework itself
    * pip install robotframework
* Install additional libraries
    * pip install robotframework-seleniumlibrary
* Install intellibot plugin for your IDE for keywords highlighting



## Useful links

Robot Framework site
https://robotframework.org/

General Robot Framework documentation 
http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html

Runner command line options 
http://robotframework.org/robotframework/latest/RobotFrameworkUserGuide.html#all-command-line-options


Selenium library documentation 
http://robotframework.org/SeleniumLibrary/SeleniumLibrary.html

BuiltIn library documentation 
http://robotframework.org/robotframework/latest/libraries/BuiltIn.html

