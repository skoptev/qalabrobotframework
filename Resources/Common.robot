*** Settings ***
Documentation  Общие ключевые слова
Library  SeleniumLibrary

*** Variables ***
${BROWSER} =  ff

*** Keywords ***
Открыть веб браузер
    open browser  about:blank  ${BROWSER}
    maximize browser window

Открыть страницу
    [Arguments]  ${url}
    go to  ${url}

Закрыть веб браузер
    close browser