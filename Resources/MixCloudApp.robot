*** Settings ***
Documentation  Описание ключевых слов для веб сайта Mixcloud
Resource  Common.robot
Resource  PO/MainPage.robot
Resource  ../Data/TestData.robot


*** Keywords ***
Открыть главную страницу сайта
    Открыть страницу  ${BASE_URL}


Нажать кнопку "Вход" в хедере страницы
    MainPage.Нажать кнопку "Вход"


Ввести логин
    [Arguments]  ${username}
    MainPage.Заполнить поле "Логин"  ${username}

Ввести пароль
    [Arguments]  ${password}
    MainPage.Заполнить поле "Пароль"  ${password}


Проверить наличие имени пользователя на странице
    [Arguments]  ${name}
    MainPage.Проверить имя пользователя  ${name}


Проверить текст ошибки
    [Arguments]  ${error_text}
    MainPage.Проверить текст ошибки логина  ${error_text}


Залогиниться на сайте
    Открыть веб браузер
    Открыть главную страницу сайта
    Нажать кнопку "Вход" в хедере страницы
    Ввести логин  ${USERNAME}
    Ввести пароль  ${PASSWORD}
    Проверить наличие имени пользователя на странице  ${USERNAME}


Осуществить поиск
    [Arguments]  ${search}
    MainPage.Найти текст  ${search}


Открыть найденную страницу
    MainPage.Открыть первый результат поиска


Включить все миксы на странице
    MainPage.Включить все миксы


Включить первый микс
    MainPage.Включить первый микс в списке