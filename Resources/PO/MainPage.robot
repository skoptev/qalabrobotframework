*** Settings ***
Documentation  Locators and actions of the main page
Library  SeleniumLibrary


*** Variables ***
${LOGIN_LINK} =  xpath=//header//span[1]
${USERNAME_INPUT} =  xpath=//input[@name='email']
${PASSWORD_INPUT} =  xpath=//input[@name='password']
${LOGIN_SUBMIT_BUTTON} =  xpath=//form/button

${LOGIN_ERROR_MESSAGE} =  xpath=//div[@class="error-message"]

${USERNAME_HEADER} =  xpath=//span[@class="user-name"]

${SEARCH_INPUT} =  xpath=//input[@name="mixcloud_query"]
${SEARCH_1ST_RESULT} =  xpath=//section[@class="people-to-follow"]//li[1]//a[@class="username"]

${PLAY_ALL_BUTTON} =  xpath=//div[@class="profile-actions"]//button
${PLAY_BUTTON_1st_MIX} =  xpath=//section[@class="card cf"][1]//span[@class="play-button"]


*** Keywords ***
Нажать кнопку "Вход"
    click element  ${LOGIN_LINK}


Заполнить поле "Логин"
    [Arguments]  ${username}
    clear element text  ${USERNAME_INPUT}
    input text  ${USERNAME_INPUT}  ${username}


Заполнить поле "Пароль"
    [Arguments]  ${password}
    clear element text  ${PASSWORD_INPUT}
    input text  ${PASSWORD_INPUT}  ${password}
    click element  ${LOGIN_SUBMIT_BUTTON}


Проверить имя пользователя
    [Arguments]  ${name}
    Wait Until Page Contains Element  ${USERNAME_HEADER}
    ${name_actual} =  get text  ${USERNAME_HEADER}
    should contain  ${name_actual}   ${name}


Проверить текст ошибки логина
    [Arguments]  ${error_text}
    wait until page contains element  ${LOGIN_ERROR_MESSAGE}
    ${error_text} =  get text  ${LOGIN_ERROR_MESSAGE}
    should contain  ${error_text}  ${error_text}


Найти текст
    [Arguments]  ${search}
    click element  ${SEARCH_INPUT}
    clear element text  ${SEARCH_INPUT}
    input text  ${SEARCH_INPUT}  ${search}


Открыть первый результат поиска
    sleep  1s
    Wait Until Page Contains Element  ${SEARCH_1ST_RESULT}
    click element  ${SEARCH_1ST_RESULT}


Включить все миксы
    Wait Until Page Contains Element  ${PLAY_ALL_BUTTON}
    click element  ${PLAY_ALL_BUTTON}
    sleep  30s


Включить первый микс в списке
    Wait Until Page Contains Element  ${PLAY_ALL_BUTTON}
    click element  ${PLAY_ALL_BUTTON}
    sleep  30s