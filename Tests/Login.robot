*** Settings ***
Documentation  Verify User can login
Resource  ../Resources/MixCloudApp.robot
Resource  ../Resources/Common.robot
Resource  ../Data/TestData.robot

Test Setup  Открыть веб браузер
Test Teardown  Закрыть веб браузер

# robot -d Results tests/Login.robot

*** Test Cases ***
Login with correct credentials
    Открыть главную страницу сайта
    Нажать кнопку "Вход" в хедере страницы
    Ввести логин  ${USERNAME}
    Ввести пароль  ${PASSWORD}
    Проверить наличие имени пользователя на странице  ${USERNAME}


Login with incorrect credentials
    Открыть главную страницу сайта
    Нажать кнопку "Вход" в хедере страницы
    Ввести логин  ${USERNAME}
    Ввести пароль  wrongPassword
    Проверить текст ошибки  ${WRONG_PASSWORD_ERROR_TEXT}

