*** Settings ***
Documentation  Verify User can play mix
Resource  ../Resources/MixCloudApp.robot
Resource  ../Resources/Common.robot
Resource  ../Data/TestData.robot

Suite Setup  Залогиниться на сайте
Suite Teardown  Закрыть веб браузер

# robot -d Results tests/PlayMix.robot

*** Test Cases ***
Play all mixes
    Осуществить поиск  ${SEARCHPHRASE}
    Открыть найденную страницу
    Включить все миксы на странице


Play one mix
    Открыть главную страницу сайта
    Осуществить поиск  ${SEARCHPHRASE}
    Открыть найденную страницу
    Включить первый микс